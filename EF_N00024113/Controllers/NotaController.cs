﻿using System;
using System.Collections.Generic;
using System.Linq;
using EF_N00024113.Models;
using EF_N00024113.Repositorio;
using EF_N00024113.Servicios;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EF_N00024113.Controllers
{

    [Authorize]
    public class NotaController : Controller
    {
        private readonly INotaRepo context;
        private readonly IClaimService claim;
        public NotaController(INotaRepo context, IClaimService claim)
        {
            this.context = context;
            this.claim = claim;
        }
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Etiquetas = context.GetEtiquetas();
            ViewBag.Etiquetitas = context.GetEtiquetaNotas();
            return View("Index");
        }

        [HttpGet]
        public IActionResult _Index(string search)
        {
            claim.SetHttpContext(HttpContext);
            var notas = context.GetNotas(claim.GetLoggedUser().Id);
            ViewBag.Etiquetas = context.GetEtiquetas();
            ViewBag.Etiquetitas = context.GetEtiquetaNotas();

            if (!String.IsNullOrEmpty(search))
            {
                notas = notas.Where(o => o.Titulo.Contains(search) || o.Contenido.Contains(search)).ToList();
                return View(notas);
            }
            return View("_Index", notas);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Etiquetas = context.GetEtiquetas();
            return View("Create", new Nota());
        }

        [HttpPost]
        public IActionResult Create(Nota nota, List<int> etiqueta)
        {
            nota.Fecha = DateTime.Now;
            List<EtiquetaNota> etic = new List<EtiquetaNota>();
            
            if (etiqueta.Count() == 0)
                ModelState.AddModelError("etiqueta", " Seleccionar por lo menos uno");

            claim.SetHttpContext(HttpContext);
            nota.IdUsuario = claim.GetLoggedUser().Id;
            if (ModelState.IsValid)
            {
                context.GuardarNota(nota);
                foreach(var item in etiqueta)
                {
                    var etique = new EtiquetaNota();
                    etique.IdEtiqueta = item;
                    etique.IdNota = nota.Id;
                    etic.Add(etique);
                }
                context.GuardarEtiqueNota(etic);
                return RedirectToAction("Index");
            }
            else
            {
                Response.StatusCode = 400;
                ViewBag.Etiquetas = context.GetEtiquetas();
                return View("Create", nota);
            }
        }

        [HttpGet]
        public IActionResult Edit(int Id)
        {

            ViewBag.Etiquetas = context.GetEtiquetas();
            var nota = context.GetNota(Id);
            return View("Edit", nota);

        }

        [HttpPost]
        public IActionResult Edit(Nota nota)
        {
            nota.Fecha = DateTime.Now;
            claim.SetHttpContext(HttpContext);
            nota.IdUsuario = claim.GetLoggedUser().Id;

            if (ModelState.IsValid)
            {
                context.ActualizarNota(nota);
                return RedirectToAction("Index");

            }
            else
            {
                Response.StatusCode = 400;
                ViewBag.Etiquetas = context.GetEtiquetas();
                return View("Edit", nota);
            }
        }

        [HttpGet]
        public IActionResult Detalle(int id)
        {
            ViewBag.Etiquetas = context.GetEtiquetas();
            ViewBag.Etiquetitas = context.GetEtiquetaNotas();
            var nota = context.GetNota(id);
            return View("Detalle", nota);
        }

        [HttpGet]
        public IActionResult Eliminar(int id)
        {
            context.EliminaNota(id);
            return RedirectToAction("Index");
        }

    }
}