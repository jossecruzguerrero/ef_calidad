﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using EF_N00024113.Models;
using EF_N00024113.Repositorio;
using EF_N00024113.Servicios;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace EF_N00024113.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly IUsuarioRepo context;
        private readonly IConfiguration configuration;
        public IClaimService claim;

        public UsuarioController(IUsuarioRepo context, IClaimService claim, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
            this.claim = claim;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View("Login");
        }
        [HttpPost]
        public ActionResult Login(string NombreUsuario, string Password)
        {
            var usuario = context.GetUsuario(NombreUsuario, CreateHash(Password));

             if (usuario != null)
             {
                var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, NombreUsuario)
                };

                var claimsIdentity = new ClaimsIdentity(claims, "Login");
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                claim.SetHttpContext(HttpContext);
                claim.Login(claimsPrincipal);

                return RedirectToAction("Index", "Home");
            }

            ModelState.AddModelError("Login", "Usuario o contraseña incorrectos.");
            return View("Login");
        }
        [HttpGet]
        public ActionResult Logout()
        {
            claim.SetHttpContext(HttpContext);
            claim.Logout();
            return View("Login");
        }
        private string CreateHash(string input)
        {
            var sha = SHA256.Create();
            input += configuration.GetValue<string>("Token");
            var hash = sha.ComputeHash(Encoding.Default.GetBytes(input));

            return Convert.ToBase64String(hash);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }
        [HttpPost]
        public ActionResult Create(Usuario usuario, string passwordpok )
        {
            if (usuario.Password != passwordpok) // <-- para convalidar contraseña y confirmacion de contraseña
                ModelState.AddModelError("Passwordpok", "Las contraseñas no coinciden");

            if (ModelState.IsValid)
            {
                usuario.Password = CreateHash(usuario.Password);
                context.SaveUsuario(usuario);
                return RedirectToAction("Login");
            }
            return View("Create", usuario);
        }
    }
}
