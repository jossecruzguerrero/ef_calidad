﻿using EF_N00024113.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EF_N00024113.Repositorio
{

    public interface IUsuarioRepo
    {
        Usuario GetUsuario(string username, string password);
        void SaveUsuario(Usuario user);
        List<Usuario> GetUsuarios();
    }
    public class UsuarioRepo : IUsuarioRepo
    {
        private readonly IEf_Context context;
        private readonly IConfiguration configuration;

        public UsuarioRepo(IEf_Context context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }

        public Usuario GetUsuario(string username, string password)
        {
            return context.Usuarios.Where(o => o.NombreUsuario == username && o.Password == CreateHash(password)).FirstOrDefault();
        }

        public List<Usuario> GetUsuarios()
        {
            return context.Usuarios.ToList();
        }

        public void SaveUsuario(Usuario user)
        {
            user.Password = CreateHash(user.Password);
            context.Usuarios.Add(user);
            context.SaveChanges();
        }

        protected string CreateHash(string input)
        {
            var sha = SHA256.Create();
            input += configuration.GetValue<string>("Token");
            var hash = sha.ComputeHash(Encoding.Default.GetBytes(input));

            return Convert.ToBase64String(hash);
        }
    }
}
