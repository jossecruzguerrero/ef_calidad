﻿using EF_N00024113.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_N00024113.Repositorio
{
    public interface INotaRepo
    {
        List<Etiqueta> GetEtiquetas();
        Nota GetNota(int id);
        List<EtiquetaNota> GetEtiquetaNotas();
        List<EtiquetaNota> GetEtiquetaNotaUsuario(int IdUsuario, int IdCategoria);
        List<Nota> GetNotas(int idUser);
        void GuardarNota(Nota nota);
        void ActualizarNota(Nota nota);
        void GuardarEtiqueNota(List<EtiquetaNota> et);
        void EliminaNota(int id);
        void EliminarEtiquetas(int id);
    }
    public class NotaRepo : INotaRepo
    {
        private readonly Ef_Context context;

        public NotaRepo(Ef_Context context)
        {
            this.context = context;

        }
        public Nota GetNota(int id)
        {
            return context.Notas.Where(o => o.Id == id).FirstOrDefault();
        }

        public List<EtiquetaNota> GetEtiquetaNotas()
        {
            return context.EtiquetaNotas.Include(o => o.Etiqueta).ToList();
        }

        public List<Etiqueta> GetEtiquetas()
        {
            return context.Etiquetas.ToList();
        }
        public List<Nota> GetNotas(int idUser)
        {
            return context.Notas.Where(o => o.IdUsuario == idUser).ToList();

        }
        public void GuardarNota(Nota nota)
        {
            context.Notas.Add(nota);
            context.SaveChanges();
        }
        public void GuardarEtiqueNota(List<EtiquetaNota> et)
        {
            context.EtiquetaNotas.AddRange(et);
            context.SaveChanges();
        }
        public void EliminaNota(int id)
        {
            var nota = context.Notas.Where(o => o.Id == id).FirstOrDefault();
            var etiqueta = context.EtiquetaNotas.Where(o => o.IdNota == id).ToList();
            context.Notas.Remove(nota);
            context.EtiquetaNotas.RemoveRange(etiqueta);
            context.SaveChanges();

        }
        public void ActualizarNota(Nota nota)
        {
            context.Notas.Update(nota);
            context.SaveChanges();
        }

        public void EliminarEtiquetas(int id)
        {
            var etiquetta = context.EtiquetaNotas.Where(o => o.Id == id).FirstOrDefault();
            context.EtiquetaNotas.RemoveRange(etiquetta);
        }

        public List<EtiquetaNota> GetEtiquetaNotaUsuario(int IdUsuario, int IdCategoria)
        {
            return context.EtiquetaNotas.Include(o => o.Etiqueta)
                .Include(o => o.Nota)
                .Where(o => o.Nota.IdUsuario == IdUsuario && o.IdEtiqueta == IdCategoria).ToList();
        }
    }
}
