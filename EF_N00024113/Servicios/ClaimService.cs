﻿using EF_N00024113.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EF_N00024113.Servicios
{
    public interface IClaimService
    {
        Usuario GetLoggedUser();
        void SetHttpContext(HttpContext httpContext);
        void Logout();
        void Login(ClaimsPrincipal principal);
    }

    public class ClaimService : IClaimService
    {
        private Ef_Context context;
        private HttpContext httpContext;

        public ClaimService(Ef_Context context)
        {
            this.context = context;
        }

        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }

        public Usuario GetLoggedUser()
        {
            var claim = httpContext.User.Claims.FirstOrDefault();
            var user = context.Usuarios.Where(o => o.NombreUsuario == claim.Value).FirstOrDefault();
            return user;
        }

        public void Logout()
        {
            httpContext.SignOutAsync();
        }

        public void Login(ClaimsPrincipal principal)
        {
            httpContext.SignInAsync(principal);
        }
    }
}
