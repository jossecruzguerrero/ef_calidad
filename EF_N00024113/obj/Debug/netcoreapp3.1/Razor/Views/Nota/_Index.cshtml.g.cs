#pragma checksum "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0827c6ecc3485206c7fec06068e5e7548bec5d0b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Nota__Index), @"mvc.1.0.view", @"/Views/Nota/_Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\_ViewImports.cshtml"
using EF_N00024113;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\_ViewImports.cshtml"
using EF_N00024113.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0827c6ecc3485206c7fec06068e5e7548bec5d0b", @"/Views/Nota/_Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7a144089b0475a2dbebe4db1f77fcda87458937d", @"/Views/_ViewImports.cshtml")]
    public class Views_Nota__Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
  
    Layout = null;
    var nota = (List<Nota>)Model;

#line default
#line hidden
#nullable disable
            WriteLiteral("\n");
#nullable restore
#line 6 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
 foreach (var item in nota)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class=\"container-fluid\">\n\n        <ul class=\"list-group\">\n            <li class=\"list-group-item\">\n                <br />\n                <span>\n                    <a");
            BeginWriteAttribute("href", " href=\"", 265, "\"", 297, 2);
            WriteAttributeValue("", 272, "/Nota/Detalle?Id=", 272, 17, true);
#nullable restore
#line 14 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
WriteAttributeValue("", 289, item.Id, 289, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral("><strong>");
#nullable restore
#line 14 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
                                                           Write(item.Titulo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong></a>\n                    <br />\n                    <span>\n                        <strong>Cuerpo: </strong>\n");
#nullable restore
#line 18 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
                         if (item.Contenido.Length > 50)
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                            <parcial>");
#nullable restore
#line 20 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
                                Write(item.Contenido.Substring(0, 50));

#line default
#line hidden
#nullable disable
            WriteLiteral(" ...</parcial> ");
#nullable restore
#line 20 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
                                                                                    }
                        else
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                            <parcial>");
#nullable restore
#line 23 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
                                Write(item.Contenido);

#line default
#line hidden
#nullable disable
            WriteLiteral("</parcial>\n");
#nullable restore
#line 24 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                    </span>\n                </span>\n\n                <br />\n                <div align=\"right\">\n                    <a");
            BeginWriteAttribute("href", " href=\"", 884, "\"", 907, 2);
            WriteAttributeValue("", 891, "/Notaid=", 891, 8, true);
#nullable restore
#line 30 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
WriteAttributeValue("", 899, item.Id, 899, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" style=\"margin-left:20px;\" class=\"btn btn-success\">Compartir</a>\n                    <a");
            BeginWriteAttribute("href", " href=\"", 995, "\"", 1028, 2);
            WriteAttributeValue("", 1002, "/Nota/Eliminar?id=", 1002, 18, true);
#nullable restore
#line 31 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
WriteAttributeValue("", 1020, item.Id, 1020, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" style=\"margin-left:20px;\" class=\"btn btn-success\">Eliminar</a>\n                    <a style=\"margin-right:20px;\" class=\"btn btn-primary float-lg-right\" href=\"#\" data-toggle=\"modal\" data-target=\"#edit\"");
            BeginWriteAttribute("onclick", " onclick=\"", 1230, "\"", 1294, 7);
            WriteAttributeValue("", 1240, "selCuenta(\'", 1240, 11, true);
#nullable restore
#line 32 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
WriteAttributeValue("", 1251, item.Id, 1251, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1259, "\',\'", 1259, 3, true);
#nullable restore
#line 32 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
WriteAttributeValue("", 1262, item.Titulo, 1262, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1274, "\',\'", 1274, 3, true);
#nullable restore
#line 32 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
WriteAttributeValue("", 1277, item.Contenido, 1277, 15, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1292, "\')", 1292, 2, true);
            EndWriteAttribute();
            WriteLiteral(">Editar</a>\n                </div>\n            </li>\n        </ul>\n        <br />\n    </div>\n");
#nullable restore
#line 38 "C:\Users\Pc\Desktop\CALIDAD FINAL\Examen-Final-Diars-master\Examen-Final-Diars-master\EF_N00024113\Views\Nota\_Index.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<!-- Modal -->
<div class=""modal fade"" id=""edit"" tabindex=""-1"" aria-labelledby=""edit"" aria-hidden=""true"">
    <div class=""modal-dialog"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""exampleModalLabel"">Editar Nota</h5>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"" id=""editNotita"">
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Cancelar</button>
                <button type=""button"" class=""btn btn-primary"" onclick=""Editando()"">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script>
    function selCuenta(id, titulo, contenido) {
        $('#mtxtId').val(id);
        $('#mtxTitulo').val(titulo);
        $('#mtxContedido').val(contenido);
");
            WriteLiteral(@"    };

    $.ajax({
        url: '/Nota/Edit',
        type: 'get'
    }).done(function (html) {
        $('#editNotita').html(html);
    })

    function Editando() {
        $('#editNota').submit();
    }

    function editar(event) {
        event.preventDefault();
        var dataString = new FormData($('#edit form')[0]);
        $.ajax({
            url: ""/Nota/Edit"",
            type: ""post"",
            processData: false,  // Important!
            contentType: false,
            cache: false,
            data: dataString
        }).done(function (html) {
            location.reload();
        }).fail(function (html) {
            $('#editNotita').html(html.responseText);
        });
    }
</script>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
