using EF_N00024113.Controllers;
using EF_N00024113.Models;
using EF_N00024113.Repositorio;
using EF_N00024113.Servicios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;

namespace EF_N00024113Test
{
    [TestFixture]
    class UsuarioTest
    {
        [Test]
        public void LoginGet()
        {
            var controller = new UsuarioController (null, null, null);
            var view = controller.Login() as ViewResult;

            Assert.AreEqual("Login", view.ViewName);
        }
        [Test]
        public void LoginPostbien()
        {
            var repo = new Mock<IUsuarioRepo>();
            repo.Setup(o => o.GetUsuario("User", "user")).Returns(new Usuario());

            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());

            var controller = new UsuarioController(repo.Object, claim.Object, null);
            var view = controller.Login("User", "user") as RedirectToActionResult;

            Assert.AreEqual("Index", view.ActionName);
        }

        [Test]
        public void LoginPostMal()
        {
            var repo = new Mock<IUsuarioRepo>();
            repo.Setup(o => o.GetUsuario("User", "user")).Returns(new Usuario());

            var claim = new Mock<IClaimService>();

            var controller = new UsuarioController(repo.Object, claim.Object,null);
            var view = controller.Login(null, null) as ViewResult;

            Assert.AreEqual("Login", view.ViewName);
        }

        [Test]
        public void Logout()
        {
            var claim = new Mock<IClaimService>();

            var controller = new UsuarioController(null, claim.Object,null);
            var view = controller.Logout() as ViewResult;

            Assert.AreEqual("Login", view.ViewName);
        }

        [Test]
        public void CreateGet()
        {
            var controller = new UsuarioController(null, null,null);
            var view = controller.Create() as ViewResult;

            Assert.AreEqual("Create", view.ViewName);
        }

        [Test]
        public void CreatePostBien()
        {

            var configura = new Mock<IConfiguration>();
            var repo = new Mock<IUsuarioRepo>();
            repo.Setup(o => o.GetUsuario("hola", "usuario")).Returns(new Usuario());
            repo.Setup(o => o.SaveUsuario(new Usuario()));
            var claim = new Mock<IClaimService>();

            var controller = new UsuarioController(repo.Object, claim.Object, configura.Object);
            var view = controller.Create(new Usuario() { Password = "user" }, "user") as RedirectToActionResult;

            Assert.AreEqual("Login", view.ActionName);
        }

        [Test]
        public void CreatePostMal()
        {
            var repo = new Mock<IUsuarioRepo>();
            repo.Setup(o => o.GetUsuario("hola","usuario")).Returns(new Usuario());

            var claim = new Mock<IClaimService>();

            var controller = new UsuarioController(repo.Object, claim.Object,null);
            var view = controller.Create(new Usuario() { Password = "1223" }, "1234") as ViewResult;

            Assert.AreEqual("Create", view.ViewName);
        }
    }
}