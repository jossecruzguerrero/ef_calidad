﻿using EF_N00024113.Controllers;
using EF_N00024113.Models;
using EF_N00024113.Repositorio;
using EF_N00024113.Servicios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace EF_N00024113Test
{
    [TestFixture]
    public class NotaTest
    {
        [Test]
        public void Index()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepo>();
            repo.Setup(o => o.GetEtiquetas()).Returns(new List<Etiqueta>());
            repo.Setup(o => o.GetEtiquetaNotas()).Returns(new List<EtiquetaNota>());
            repo.Setup(o => o.GetNotas(1)).Returns(new List<Nota>());

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Index() as ViewResult;

            Assert.AreEqual("Index", view.ViewName);
        }

        [Test]
        public void Detalle()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepo>();
            repo.Setup(o => o.GetEtiquetas()).Returns(new List<Etiqueta>());
            repo.Setup(o => o.GetEtiquetaNotas()).Returns(new List<EtiquetaNota>());
            repo.Setup(o => o.GetNota(1)).Returns(new Nota());

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Detalle(1) as ViewResult;

            Assert.AreEqual("Detalle", view.ViewName);
        }

        [Test]
        public void Create()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepo>();
            repo.Setup(o => o.GetEtiquetas()).Returns(new List<Etiqueta>());

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Create() as ViewResult;

            Assert.AreEqual("Create", view.ViewName);
        }

        [Test]
        public void CreatePostA()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepo>();
            repo.Setup(o => o.GuardarNota(new Nota()));
            repo.Setup(o => o.GuardarEtiqueNota(new List<EtiquetaNota>()));

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Create(new Nota(), new List<int>() { 1 }) as RedirectToActionResult;

            Assert.AreEqual("Index", view.ActionName);
        }

        [Test]
        public void Edit()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepo>();
            repo.Setup(o => o.GetEtiquetas()).Returns(new List<Etiqueta>());

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Edit(1) as ViewResult;

            Assert.AreEqual("Edit", view.ViewName);
        }

        [Test]
        public void EditPostA()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepo>();
            repo.Setup(o => o.EliminarEtiquetas(1));
            repo.Setup(o => o.ActualizarNota(new Nota()));
            repo.Setup(o => o.GuardarEtiqueNota(new List<EtiquetaNota>()));

            var controller = new NotaController(repo.Object, claim.Object);
            var view = controller.Edit(new Nota()) as RedirectToActionResult;

            Assert.AreEqual("Index", view.ActionName);
        }


        [Test]
        public void Eliminar()
        {
            var claim = new Mock<IClaimService>();
            claim.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repo = new Mock<INotaRepo>();

            var controller = new NotaController(repo.Object, claim.Object);
            controller.Eliminar(1);

            Assert.AreEqual(null, null);
        }
    }
}
